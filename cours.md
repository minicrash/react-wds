# React
(Faire plus de petit exercices afin de mieux appréhender les concepts pour ensuite passer à une application plus complète)
# Introduction

React est devenu de plus en plus populaire dans le monde comme librairie javascript afin de construire des applications web performantes. Notamment depuis 2009, l'écosystème Javascript a explosé depuis l'arrivé de NodeJS. Tous les ans voir tous les deux des nouvelles librairies/Framework arrivent sur le marché. Depuis plusieurs React se démarque c'est pouruqoi nous allons l'étudier.

## C'est quoi React ?

React est une librairie front-end créé par Facebook en 2013. React va permettre la gestion de l'UI. Il va être couplé à d'autres librairie de son écosystème.

### A quoi sert React et les autres framework web ?

Tout d'abord, c'est une librairie front end, c'est un ensemble de class, fonctions, qui facilite la création d'applications web. Les autres framework web sont surtout Angular et VueJS pour les plus récents et les plus populaires à ce jour.

### Pourquoi apprendre React en 2019

Schéma <a href="https://https://2018.stateofjs.com/front-end-frameworks/overview/" target="blank">State of JS</a>

Des sites, comme Airbnb, Facebook, Instagram, Discord, Dropbox, Atlassian etc utilise React et l'écosystème React avec React Native (Application mobile IOS et Android). React comme VueJS sont bien partie pour durée quelques années encore. De plus, C'est en phase avec le marché du travail actuel. Sur une page de indeed, e nmot clé Développeur secteur bretagne plus de 50% des postes contiennent React et/ou VueJS. C'est pourquoi il faut former à l'un des deux et nous cette semaine c'est React.

### Une approche basée composant

React a mis au gout du jour, le paradigme composant. En effet, cette approche existait depuis le logiciel. A l'arrivé du web, celui-ci à été conçu en tant que document et non application. Très vite nos site devenait des immenses plats de spaghetti imcompréhensible et difficile à maintenir. React a fait éclater les séparation stricte HTML CSS et JS. Pour ce recentrer sur des composants autonomes et complet. Les concurrents comme Angular et VueJS fonctionne de même.

#### Encapsulation

Un composant React est comme une boite noire. Ils disposent d'une API contenant tout le nécessaire au bon fonctionnement : La structure, les styles et le comportement. Ces 3 volets sont écrits dans un seul et même fichier écrit en JSX. Pour connaitre le comportenement d'un composant, tout se trouve dans un seul fichier, ce qui est plus facile à débugguer et à tester.

#### Composition

La force de React est d'avoir une structure sous forme d'arborescence de composants. Ceux-la sont écris de manière à être des composants réutilibles.

### Gestion d'état de l'application

Quand on développe des applications aux comportements complexes dans l'interface utilisateur une des plus grosses difficultés est la gestion de l'état applicatif. Les données manipulées dans l'interface doient évoluer en respectant des règles précises.  
Inversement, lorsque les données évolues, l'interface doit réagir pour refléter un nouvel état applicatif. Pour bien gérer cette gestion applicative, React nous force à suivre le principe de les données descendent, l'état remonte. Afin de gérer l'état applicatif, nous utiliserons les <b>props</b> et le <b>state</b>.

## Initialisation de l'environnement de développement

Un editeur de code type VS code
Installation de NodeJS

## Première application React

Dans cette première application React, on va importer le CDN React à notre fichier HTML.
Création d'un fichier index.html avec un doctype de base.
Ajouter cette div dans le body
`<div id="like_button_container"></div>`

Ajouter ces balises script important react et le futur premier composant.

```html
<script src="https://unpkg.com/react@16/umd/react.development.js" crossorigin></script>
<script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js" crossorigin></script>
<script src="index.js"></script>
```

Dans le fichier index.js ajouter ce composant bouton

```jsx
"use strict"
const e = React.createElement
class LikeButton extends React.Component {
  constructor(props) {
    super(props)
    this.state = { liked: false }
  }
  render() {
    if (this.state.liked) {
      return "You liked this."
    }
    return e("button", { onClick: event => this.setState({ liked: true }) }, "Like")
  }
}
```

Ici on a créé un bouton, quand on clique dessus celui-ci se transforme en chaine de caractère. Maintenant, il faut lier ce composant à notre DOM.

```js
const domContainer = document.querySelector("#like_button_container")
ReactDOM.render(e(LikeButton), domContainer)
```

Ouvrez le projet dans votre navigateur. Voila, nous avons créé notre premier composant React. Avouez que c'est pas sexy d'écrire su React. Rassurez vous, on ne vas pas utiliser ça. On a va utiliser la ligne de commande pour générer notre premier projet avec le Create React App

## Create React App

Le Create React App permet de générer le squelette d'une application web. D'autant plus qu'on va utiliser la notation ES6 pour coder nos composants et du JSX. Langage que le navigateur ne peut interpréter. Pour fonctionner, on doit avoir une version de node >= 6 et npm >= 5.2. Plus c'est récent mieux c'est. Une fois node installé,
`$ npm install --global create-react-app` cette commande permet d'installer le create react app au global dans votre ordinateur et donc peut être utilisé n'importe où. On va créer un nouveau projet : `create-react-app monProjet`

Une fois l'installation du projet, finit, il y a plusieurs commandes indiquées :

- npm start : démarrer le serveur de développement
- npm run build : Builder les fichiers pour les rendre static pour être envoyer en production.
- npm run test : Lancer les tâches de tests
- npm run eject : Supprime l'outil, et copie toutes les dépendances, les fichiers de configurations dans un dossier. Aucun retour en arrière n'est possible.

Lancement du projet !!

Ouvrez le navigateur pour voir le projet lancé. Votre seconde application React créée. Décortiquons la structure du projet générer.

## Structure d'un projet React

Dans ce projet généré, il est composé de 3 dossiers :

- node_modules : Le dossier contenant toutes les dépendances du projet
- public : Le dossier ou les ressources du projet sont publiques, comme les images, les fonts etc.
- src : le dossier contenant tous le code sources du projet.

Aux côtes des dossiers, il y a quelques fichiers :

- .gitignore : Avec l'outil de versionning git, vous pourrez ignorez des fichiers pour qu'il reste uniquement sur votre ordinateur, comme des fichiers de configuration.
- package.json : Ce fichier est en complément du dossier node_modules. On va trouver toutes les caratéristiques du projet ainsi que les dépendances et leurs versions utilisées.
- README.md : Ce fichier permet d'indiquer les informations de mise en route du projet ou tous d'informations utiles.

Dans le dossier src/ il y a le point d'entrée de l'application `index.js` et un premier composant `App.js`.

Notre projet est en place, il y a plus qu'a le réaliser.

## Rappel sur la notation ES6

Avec React comme Vue, on ecrit du Javascript avec la notation basée sur le standard ECMAScript 2015, ES6. Cette notation permet de faire certaines choses avec le Javascript qui était compliqué dans le passé. De plus, on peut avoir une vrai culture objet. Voici quelques exemples ES6 le plus utilisés.

### Class

En ES6, on peut ecrire des classes avec le mot clé `class` celles-ci peux hériter de classe mère grâce à `extend`

### Fonctions fléchées

Autre nouveauté intéressante avec ES6, l'écritures du fonctions ont été raccourcies. En effet, une boucle `map` le mot clé fonctione est remplacé par `=>`.  
En JS natif :

```javascript
let arr = [1, 2, 3, 4]
arr.map(function(item, index) {
  console.log(item, index)
})
```

En ES6 :

```javascript
let arr = [1, 2, 3, 4]
arr.map((item, index) => console.log(item, index))
```

Cela permet aussi une meilleur expression du `this`

### Destructuration

La destructuration permet d'attribuer des variables d'un objet et/ou tableau, exemple :

```javascript
var obj = {
  firstname: "Dark",
  lastname: "Vador"
}
var firstname = obj.firstname
var lastname = obj.lastname

const { firstname, lastname } = this.props
```

### Import/Export

Tout notre application est découpé en composant ou module, Pour pouvoir les utiliser, nous devons nous baser sur la syntaxe officielle, ES MODULES. Pour utiliser un module dans un autre module, celui-ci devra être exporter puis importer dans le module nécessaire.

```js
//convert.js
export function eurosToDollars(euros) {
  if (typeof euros === "string") euros = parseFloat(euros)
  return euros * 1.135
}
//index.js
import { eurosToDollar } from "./convert.js"
console.log(eurosToDollar(3))
```

## Découpe en composant

Une des plus grosse difficulté avec React, comme Vue ou Angular, c'est de déterminer le bon découpage en composant de son application.
Pour ma part, je pense qu'il faut séparer les composants UI des composants métier. Il faut faire attention à ne pasfaire des composants trop générique. Un exemple, vouloir faire des input de formulaire générique.
https://www.compteco2.com/ on va indiquer les composants comment ils sont découpés. Uniquement d'un point de vue UI.

Ensuite, pour la partie métier, il y a plusieurs bonnes pratiques. Un exemple sur lequel j'ai travaillé sur une application mobile :
On a découpé nos composants en 3 dossiers :

- Views : Les composants présents dans views permettent d'afficher la structure d'un écran. Ce composant appelle le container
- Containers : Les containers sont des composant qui vont gérer la partie métier et l'envoie au serveur. Il va envoyer les données à afficher aux composants dans components.
- Components : Les components vont gérer l'UI et faire remonter les data aux containers.

## Composant Stateless

Les composants dis Stateless, sont des composants qui non pas de gestions d'état. Il sont aussi appelé fonction pur. Il vont recevoir des props et les afficher. Les props sont des données que l'on va envoyer aux composants enfant. Ces données peuvent de n'importe quel type, chaine de cractère, entier, tableau, objet, booléen ...

```jsx
//Header.js
function Header({props}) {
  return <div> { props.title}</div>
}
export default Header

//App.js
import Header from './Header'
{...}
render(){
  return <Header title={"Hello"} />
}
```

## Style avec React

## Expressions

Avec React, il est possible d'utiliser des conditions dans le JSX. c'est possible en remplaçant les conditions classiques par les opérateurs logique (&& || et le ternaire ? : )

### Si ... Alors ...

On utilisera l'opérateur logique && : `<li>{user.admin && <a href="/admin">Admin</a>}</li>`

### Si ... Alors ... Sinon ...

On utilisera l'opérateur ternaire :

```jsx
<li>{user.admin ? <a href="/admin">Admin</a> : <a href="/other">Other</a>}</li>
```

## Boucles

Pour utilisez des boucles dans le JSX, on va utilisez la fonction `map()` exemple :

```js
const numbers = [1, 2, 3, 4]
const doubles = numbers.map(x => x * 2) // [2, 4, 6, 8]
```

Dans le map, on utilisera les arrow function pour ecrire plus vite et pour avoir plus de clarté. Quand on voudra parcourir un tableau d'objet dans le JSX, on devra ajouter un props à l'item enfant. En effet, React utilise un DOM virtuel, Ce props va permettre à React de s'y retrouver quand on ajoutera/supprimera un élément de la liste. Ce props doit être unique et stable dans le temps.

```js
const users = [
  { id: 1, name: 'Alice' },
  { id: 2, name: 'Bob' },
  { id: 3, name: 'Claire' },
  { id: 4, name: 'David' },
]
render () {
  return (
    <div>
      {this.props.users.map(({ id, name }) => (
        <a href={`/users/${id}`} key={id}>{name}</a>
      ))}
    </div>
  )
}
```

Key

## Props

Un props est toujours passé du parent à l'enfant, Il est toujours considéré en lecture seule dans le composant enfant.

Il existe 3 props dis techique :

- key : est utilisé dans la manipulation pour que React gère chaque élément d'un tableau. N'est pas consultable
- children : C'est les composants dans un composant wrapper
- dangerouslySetInnerHTML : Permet d'injecter un balisage HTML particuler

Pour faciliter le développement, on peut typer nos variables props, avec la librairie prop-types :

```jsx
import PropTypes from "prop-types"
MyComponent.propTypes = {
  firstname: PropTypes.string.isRequired,
  year: PropTypes.number
}
```

De plus, on peut implémenter des jeux de données de default grâce à `defaultProps` :

```jsx
MyComponent.defaultProps = {
  firstname: "John",
  year: 2018
}
```

## Composant en fonction ou composant en class ?

Si votre composant n'a pas de gestion d'état, juste du passage de props, utilisez un composant en fonction. Par contre, si il y a une gestion d'état, ou agir sur le cycle de vie du composant, un composant en classe est à utilisé.
Il est conseillé d'utiliser le plus possible les composants en fonctions car il apporte de meilleur performance.

## This

Passage par référence
https://openclassrooms.com/fr/courses/4664381-realisez-une-application-web-avec-react-js/4664866-faites-reference-au-bon-this-dans-vos-fonctions

## State

L'état local ou state est l'état du composant. En effet, un composant possède plusieurs états en fonctions d'événements et/ou de données envoyées. L'état d'un composant est propre au composant, il est interne. Il s'initialise depuis le constructeur du composant. En fonction d'un événement quel qu'il soit, on peut modifier l'état du composant avec la méthode `setState()`.

## Navigation

Un des points importants dans une application, c'est de mettre en place la navigation. Pour cela on utilise le plugin, `react-router-dom`. Une fois installé le package, il reste plus qu'a configurer le routing. la doc de react-router-dom : https://reacttraining.com/react-router/web/guides/quick-start
On va commencer par initialiser nos routes :

```jsx
//App.js
import { BrowserRouter as Router, Route } from "react-router-dom"
import MyComponent from './src/components/MyComponent'
[...]
<Link to="/">Home</Link>
[...]
<Route path="/" component={MyComponent}  />
```

## Redux

Redux est un système de centralisation de donnée. Avec React il y a deux types de données :

- Les props : données accessibles en lecture seule
- Le state : données disponible en lecture/écriture interne à un composant

Le problème est donc le partage de donnée dans entre plusieurs composants. Redux apporte un notion de store. On va pouvoir stocker les informations des composants qui y sont rattachés. Quand le composant A lance une action et entraine une modification du store, le composant B sera informé de le nouvelle donnée.

https://redux.js.org/introduction/getting-started
