# Devoir

## Règle du pendu

Le jeu du pendu consiste à faire deviner un mot ou une phrase, en essayant tour à tour des lettres possibles. Chaque essai révèle les lettres correspondantes dans le schéma du texte à deviner.

Dans le jeu classique, chaque essai infructueux (lettre non utilisée) fait progresser le dessin d’un pendu, et lorsque le dessin est complet (tête, corps, membres), on a perdu. C’est un jeu à deux, tour par tour, et la personne qui réalise l’ultime mauvais essai a perdu.

## Simplification des règles

Toute la partie graphique du pendu sera mis de côté. Un compteur d'essai fera l'affaire. Au bout de 10 essais la partie est perdue.
De plus, le jeu sera mono-joueur. L'alphabet utilisé sera l'alphabet latin. Pas de caractères spéciaux.
Le masque sera sympobiliser par des _underscore_ '\_'.  
Les lettres essayés seront grisés.  
Ajoutez un bouton qui lance le jeu et en cas de nombres de tentative atteintes, ce bouton servira pour relancer le jeu.

Voir l'image ci dessous
<img src="./pendu.png" width="500px" height="500px">

## Notation

Le devoir sera noté sur 20. L'essentiel est que le jeu fonctionne ensuite une découpe en composant. Je vous conseil de commencer dans un seul et même composant pour ensuite découper le jeu en plusieurs composant.

## Restitution

Pour la restitution du devoir vous avez 2 choix :

- Soit vous m'envoyez le lien de projet Github/Gitlab
- Soit vous m'envoyez un zip, sans les nodes_modules, à mon adresse email : glenn.guegan@gmail.com
