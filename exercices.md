# Exercices

Dans cette partie, chaque exercice vous menera à la construction d'une application web complète. (excepté l'exercice 0)
Vous avez le choix de la librairie de style à implémenter pour ce projet.

//Découpage de l'application en bloc

## Exercice 0

Créer un formulaire de login en React (sans utiliser le CLI) avec un champ email et mot de passe suivis d'un bouton afin d'envoyer le contenu de ce formulaire.

## Exercice 1

Avec CRA créez un nouveau projet beer-app, Le but de cette exercice est de créez des composants fonctionnelles :

- un composant Header qui aura le titre de notre application
- Un composant BeerItem, qui aura le nom, l'image et un bouton afin d'afficher plus d'information sur la bière par la suite.

## Exercice 2

Dans cette exercice, implémenter un jeu de donnée en tant que defaultProps, une liste d'objet bière qui va alimenter notre nouveau composant : BeerList. Ce composant ne sera pas un composant fonctionnel car on va implémenter des méthodes de cycle de vie, et une gestion d'état.
Voici le type de jeu de donnée à implémenter :

```json
{
  "name": "Ma bière",
  "labels": {
    "medium": "https://www.roidebretagne.com/I-Grande-53188-biere-baril-bio-originale-33cl-5-3.net.jpg"
  }
}
```

## Exercice 3

Notre jeu de donnée est implémenté, on va retsctructurer notre application afin que les datas soient envoyés depuis un composant parent à notre liste de bière. Pour cela créé un nouveau dossier `containers/BeerListContainer.js` afin de gérer l'initialisation d'une liste de bière distante depuis cette API : https://api.punkapi.com/v2/beers

## Exercice 4

Le but de cette exercice est d'utiliser la navigation. L'initialisation de celle-ci doit se faire dans le App.js.
Au clique que le button de beerItem, on doit envoyer l'utilisateur sur une nouvelle page afin de voir les informations complémentaires de la bière sélectionné.

## Exercice 5

Mettez en place une barre navigation afin de naviguer entre 2 composants :

- La liste de bière
- Un composant RandomBeer, qui aura pour but de nous afficher une bière de façon aléatoire.

## Exercice 6 
Permettre à un utilisateur de selectionner ses bières favorites. Insérer une icône étoile dans le composant BeerItem afin de pusher dans un tableau les bières favorite selectionnées.
Ce tableau doit être stocker dans le localStorage du navigateur : https://developer.mozilla.org/fr/docs/Web/API/Window/localStorage
Ensuite créer un nouveau composant dans navigation qui liste les bières favorites.
Faire en sorte que nos bières favorites soit afficher dans notre liste principale (BeerListContainer)

## Exercice 7 
Dans notre composant favoriteBeer, qui liste nos bières favorites. Si on clique sur l'icône une nouvelle fois, l'item selectionné est supprimé de notre liste de favori.
Rajoutez un bouton qui réinitialise toutes nos bières favorites. En cliquant sur ce bouton, on fait un retour arrière dans la navigation.   
Modifiez le comportement notre fonctionnalité favoriteBeer lorsque notre liste est vide.

## Exercice 8 
Mettre en place une pagination pour la liste de bière (BeerListContainer). L'api possède 240 bières au totale et la route sur laquelle appeller : https://api.punkapi.com/v2/beers?page=1&per_page=40

## Exercice 9 
1ère partie :
Nouveau projet ToDo List. Comme son nom l'indique, vous allez créer une todo list composé d'un champ input afin de remplir la liste et d'un composant qui va afficher la liste.
On peut intéragir avec cette liste pour modifier et/ou supprimer un item. Commencez par travailler avec des données dites statique. Définir un tableau en "dur". 

2ème partie : 
Votre application fonctionne, on va maintenant stocker et récupérer les données depuis une base de donnée. Pour cela créez un serveur (NodeJS/PHP/Python) connecté à une BDD de votre choix.

3ème partie : 
Connectez votre application ToDo list à votre serveur afin de remplacer les données statiques
